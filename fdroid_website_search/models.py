#!/usr/bin/env python3
#
# models.py - part of fdroid-website-search django application
# Copyright (C) 2017 Michael Pöhn <michael.poehn@fsfe.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.db import models
from django.utils.timezone import now


import datetime


class Language(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField('name', max_length=255)
    def __str__(self):
        return self.name
    class Meta():
        unique_together = ('name',)


class FDroidSite(models.Model):
    id = models.AutoField(primary_key=True)
    repoUrl = models.CharField('repository URL', max_length=255)
    siteUrl = models.CharField('website URL', max_length=255)
    iconMirrorUrl = models.CharField('repository URL for icons', max_length=255)
    lastUpdated = models.DateTimeField('last updated', default=now)
    repoTimestamp = models.DateTimeField('repo timestamp', default=datetime.datetime(1970, 1, 1, 0, 0))
    def __str__(self):
        return str(self.siteUrl).replace('http://', '').replace('https://', '')


class Category(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField('name', max_length=255) 
    def __str__(self):
        return self.name
    class Meta():
        unique_together = ('name',)


class AntiFeature(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField('name', max_length=255) 
    def __str__(self):
        return self.name
    class Meta():
        unique_together = ('name',)


class License(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField('name', max_length=255) 
    def __str__(self):
        return self.name
    class Meta():
        unique_together = ('name',)


class App(models.Model):
    id = models.AutoField(primary_key=True)

    name = models.CharField('name', max_length=255)
    packageName = models.CharField('package name', max_length=255)
    summary = models.CharField('summary', max_length=255, blank=True)
    description = models.TextField()
    whatsNew = models.TextField()

    authorName = models.CharField('authorName', max_length=255)
    authorEmail = models.CharField('authorEmail', max_length=255)

    categories = models.ManyToManyField(Category)
    antiFeatures = models.ManyToManyField(AntiFeature)
    license = models.ForeignKey(License, null=True, on_delete=models.PROTECT)

    webSite = models.CharField('webSite', max_length=255)
    sourceCode = models.CharField('sourceCode', max_length=255)
    issueTracker = models.CharField('issueTracker', max_length=255)
    changelog = models.CharField('changelog', max_length=255)

    donate = models.CharField('donate', max_length=255)
    bitcoin = models.CharField('bitcoin', max_length=255)
    litecoin = models.CharField('litecoin', max_length=255)
    flattrID = models.CharField('flattrID', max_length=255)
    liberapayID = models.CharField('liberapayID', max_length=255)

    added = models.DateTimeField()
    lastUpdated = models.DateTimeField()

    icon = models.CharField('icon', max_length=255)
    site = models.ForeignKey(FDroidSite, verbose_name="F-Droid website", on_delete=models.PROTECT)

    def __str__(self):
        return self.name
    class Meta():
        unique_together = ('packageName', 'site')

class AppI18n(models.Model):
    id = models.AutoField(primary_key=True)

    name = models.CharField('name', max_length=255, blank=True)
    summary = models.CharField('summary', max_length=255, blank=True)
    description = models.TextField(blank=True)

    whatsNew = models.TextField(blank=True)

    icon = models.CharField('icon', max_length=255, blank=True)

    lang = models.ForeignKey(Language, verbose_name="language", on_delete=models.PROTECT)
    entry = models.ForeignKey(App, verbose_name="package", on_delete=models.CASCADE)
    class Meta():
        unique_together = ('lang', 'entry')
